﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ch19Contracts;

namespace Ch19Words1
{
  public class Words1 : IWords
  {
    public string[] extract_words(string path)
    {
      var inp = Regex.Split(File.ReadAllText(path).ToLower(), "[\\W_]+");
      var stop = File.ReadAllText("../stop_words.txt").ToLower().Split(',').ToList();
      stop.AddRange("a b c d e f g h i n j l n m o p q r s t u v w x y z".Split(' '));
      return inp.Where(word => !stop.Contains(word)).ToArray();
    }
  }
}