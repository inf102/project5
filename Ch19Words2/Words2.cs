﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ch19Contracts;

namespace Ch19Words2
{
  public class Words2 : IWords
  {
    public string[] extract_words(string path)
    {
      var stops = File.OpenText("../stop_words.txt").ReadToEnd().Split(',');
      return Regex.Split(File.OpenText(path).ReadToEnd().ToLower(), "[\\W_]+")
        .Where(w => !stops.Contains(w) && w.Length > 1).ToArray();
    }
  }
}