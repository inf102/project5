﻿using System.Collections.Generic;
using System.Linq;
using Ch19Contracts;

namespace Ch19Frequencies2
{
  public class Frequencies2 : IFrequencies
  {
    public IEnumerable<KeyValuePair<string, int>> top25(IEnumerable<string> word_list)
    {
      return word_list.ToArray()
        .GroupBy(i => i).OrderByDescending(i => i.Count())
        .Select(p => new KeyValuePair<string, int>(p.Key, p.Count()))
        .Take(25);
    }
  }
}