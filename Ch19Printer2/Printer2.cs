﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ch19Contracts;

namespace Ch19Printer2
{
  public class Printer2 : IPrint
  {
    public void print(IEnumerable<KeyValuePair<string, int>> pairs)
    {
      foreach (var s in pairs.Select(w => $"{w.Key}  -  {w.Value}"))
        Printed(this,s);
    }

    public event EventHandler<string> Printed = delegate { };
  }
}