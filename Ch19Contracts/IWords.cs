﻿using System.Collections;
using System.Collections.Generic;

namespace Ch19Contracts
{
  public interface IWords
  {
    string[] extract_words(string path);
  }
}