﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Ch19Contracts
{
  public interface IFrequencies
  {
    IEnumerable<KeyValuePair<string, int>> top25(IEnumerable<string> word_list);
  }
}