﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Ch19Contracts
{
  public interface IPrint
  {
    void print(IEnumerable<KeyValuePair<string, int>> pairs);
    event EventHandler<string> Printed;
  }
}