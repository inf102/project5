﻿using System.Collections.Generic;
using System.Linq;
using Ch19Contracts;

namespace Ch19Frequencies1
{
  public class Frequencies1 : IFrequencies
  {
    public IEnumerable<KeyValuePair<string, int>> top25(IEnumerable<string> word_list)
    {
      var freqs = new Dictionary<string, int>();
      foreach (var w in word_list)
      {
        if (freqs.ContainsKey(w)) freqs[w]++;
        else freqs.Add(w, 1);
      }

      var data = freqs.ToList().OrderByDescending(p => p.Value);
      return data.Take(25);
    }
  }
}