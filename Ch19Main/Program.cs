﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Runtime.InteropServices;
using Ch19Contracts;

namespace Ch19Main
{
    class Program
    {
      static void Main(string[] args)
      {
        var data = File.ReadAllText("config.ini").Split('\n');
        var words_plugin = data[0].Split('=')[1];
        var freqs_plugin = data[1].Split('=')[1];
        var printer_plugin = data[2].Split('=')[1];

        Console.WriteLine($"Using Plugins: " +
                          $"\n Words - {words_plugin} " +
                          $"\n Frequencies - {freqs_plugin} " +
                          $"\n Printer - {printer_plugin}");

        var wordsAsm = Assembly.LoadFile(words_plugin);
        var words = new List<string>();
        foreach (var type in wordsAsm.GetTypes())
        {
          var wordsContract = type.GetInterface(typeof(IWords).ToString());
          if (wordsContract == null) continue;
          var inst = (IWords) Activator.CreateInstance(type);
          words.AddRange(inst.extract_words(args[0]));
        }

        var freqsAsm = Assembly.LoadFile(freqs_plugin);
        IEnumerable<KeyValuePair<string, int>> top25 = null;
        foreach (var type in freqsAsm.GetTypes())
        {
          var freqsContract = type.GetInterface(typeof(IFrequencies).ToString());
          if (freqsContract == null) continue;
          var inst = (IFrequencies) Activator.CreateInstance(type);
          top25 = inst.top25(words);
        }

        var printerAsm = Assembly.LoadFile(printer_plugin);
        foreach (var type in printerAsm.GetTypes())
        {
          var printerContract = type.GetInterface(typeof(IPrint).ToString());
          if (printerContract == null) continue;
          var inst = (IPrint) Activator.CreateInstance(type);
          inst.Printed += (sender, s) => Console.WriteLine(s); //this is the simplest way i can think of to output
          inst.print(top25);
        }

      }
    }
}
