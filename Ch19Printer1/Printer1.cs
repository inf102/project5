﻿using System;
using System.Collections.Generic;
using Ch19Contracts;

namespace Ch19Printer1
{
  public class Printer1 : IPrint
  {
    public void print(IEnumerable<KeyValuePair<string, int>> pairs)
    {
      foreach (var w in pairs)
        Printed(this, $"{w.Key}  -  {w.Value}");
    }

    public event EventHandler<string> Printed = delegate { };
  }
}