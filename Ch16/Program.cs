﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Ch16
{
 class Program
  {
    static void Main(string[] args)
    {
      var p = new Program(args[0]);
      p.Run();
      Console.WriteLine(p.Output());
      p.Reflect();
    }

    Assembly project1lib = Assembly.LoadFile("Project1Library.dll");
    private Type type;
    private object tf;

    public Program(string path)
    {
      type = project1lib.GetType("Project1Library.TermFrequency");
      var ctor = type.GetConstructor(new[] { typeof(string) });
      tf = ctor.Invoke(new[] {path});
    }

    public string File()
    {
      var field = type.GetField("file", BindingFlags.NonPublic
                                        | BindingFlags.Instance);
      return (string)field.GetValue(tf);
    }

    public bool Filter(string word)
    {
      var filter = type.GetMethod("filter");
      return (bool)filter.Invoke(tf, new[] { word });
    }

    public void Run()
    {
      var run = type.GetMethod("run");
      run.Invoke(tf, null);
    }

    public string Output()
    {
      return (string) type.GetMethod("output").Invoke(tf, null);
    }

   public void Reflect()
   {
     Console.WriteLine();
     Console.WriteLine("Reflection on Project1Library.dll");
     Console.WriteLine("-----");

     Console.WriteLine($"Types Available ({project1lib.GetTypes().Length})");
     foreach (var t in project1lib.GetTypes())
     {
       Console.WriteLine($"  [{t}]");

       Console.WriteLine($"   Methods({t.GetMethods().Length}):");
       foreach (var m in t.GetMethods())
         Console.WriteLine($"    {m}");

       var fields = type.GetFields(
         BindingFlags.Public
         | BindingFlags.NonPublic
         | BindingFlags.Instance);
       Console.WriteLine($"   Members({fields.Length}):");
       foreach (var f in fields)
         Console.WriteLine($"    {f} - " + (f.IsPrivate ? "Private" : "Public") );
     }
   }
  }
}
