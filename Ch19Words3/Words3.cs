﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ch19Contracts;

namespace Ch19Words3
{
    public class Words3 : IWords
    {
      public string[] extract_words(string path)
      {
          return Regex.Split(File.OpenText(path).ReadToEnd().ToLower(), "[\\W_]+")
            .Where(w => !File.OpenText("../stop_words.txt").ReadToEnd().Split(',').Contains(w) && w.Length > 1)
            .ToArray();
      }
    }
}
